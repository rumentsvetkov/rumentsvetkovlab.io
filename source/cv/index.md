---
layout: cv
title: Rumen Tsvetkov - CV
date: 2023-02-16 15:37:00

author: 'Rumen Tsvetkov'
email: 'rtsvetkov@keemail.me'
phone: '+359 878 257 550'
---
<img alt="Rumen Tsvetkov" src="r_tsvetkov.jpg" width="20%" align="left" hspace="10">


# {{ author }}
<a href="mailto:{{ email }}" target="_blank" rel="noopener" title="Send email to {{ author }}">{{ email }}</a> | <a href="tel:{{ phone }}" target="_blank" rel="noopener" title="Call to {{ author }}">{{ phone }}</a>

<a href="https://www.linkedin.com/in/rumen-tsvetkov-270142a3?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BO31rwCbOT5mkO7tiP%2FJZfg%3D%3D"><strong>LinkedIn</strong></a>

#  

## About me

I am a software developer with over 7 years of professional experience specializing in web scraping and data processing automation. Throughout my career, I have honed my skills as a passionate Python developer and a dedicated enthusiast of Linux-based systems. My expertise extends to Bash scripting, enabling me to efficiently automate complex tasks and streamline workflows. I possess an analytical mindset, strong problem-solving abilities, and a commitment to continuous learning. I am also a loyal, self-organized, and proactive professional.


## Skills
- Python
- Linux
- Bash
- MySQL
- MongoDB
- Selenium
- Pyppeteer
- Playwright
- PySpark
- Javascript
- Docker


## Work Experience

### Software Developer
#### iCOVER
##### January 2022 - August 2023

As part of the automation team, I was responsible for creating new data collection automations and maintaining the existing ones. I also played a key role in the development and support of the team's systems and frameworks, implementing third-party APIs and new features to enhance our capabilities.

***Technologies Used:*** Pyppeteer, Playwright, Selenium, MongoDB, Solr, Redis, RabbitMQ


<div class="page-break-before"></div>


### Software Developer
#### Internet Securities Bulgaria
##### October 2018 - December 2021

- Develop and maintain Data Feed (Big Data) automations;
- Develop, improve and maintain Data Feeds framework;
- Design and develop Data Feeds systems;
- Train new team members;
- Support the team in planning tasks and ad-hock additional tasks;

***Technologies used:*** Python, PySpark, Bash, Linux Server, MySQL, AWS


### Junior Software developer
#### Internet Securities Bulgaria
##### April 2016 - October 2018

- Develop and maintain automations of different business processes related to data collection and data processing;
- Initiated the development of custom software with the view of improving team’s daily operations;
- Initiated the implementation of new features into the automations frameworks;
- Trained new team members;

***Technologies used:*** Python, JavaScript


## Education

### Communication Engineering and Technologies
#### Technical University of Varna
Completed the full course of study


## Projects

### Software Developer
#### [Aptechko.bg](https://aptechko.bg)

I am responsible for developing and maintaining data collection processes, designing and developing data collection frameworks, creating system architectures for data collection, designing database architectures, and automating various business processes.

***Technologies:*** Python, JavaScript, MySQL, MongoDB


### audienceplatform
#### Audience Platform
I was responsible for creating connectors for various AD services (like Google ads, Google Analytics, Freewheel, TVSquared etc.) to facilitate data collection and data processing.

***Technologies:*** Python, Pandas, Bash, Docker, AWS, AirFlow


<div class="page-break-before"></div>


## Personal Interests
- Bulgarian Folklore Dances
- Cycling
- Travelling
- Hiking
- Puzzles
